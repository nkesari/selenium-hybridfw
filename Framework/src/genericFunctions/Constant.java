package genericFunctions;

public class Constant {
	    public static final String URL = "http://www.carguruji.com/shop/";
	    public static final String Path_TestData = "D://BUSINESS//workspace//HybridFrameworkForDemoSite//Framework//src//testData//ExcelData.xlsx";
	    
		//Test Data Sheet Columns
		public static final int Col_TestCaseName = 0;	
		public static final int Col_UserName =1 ;
		public static final int Col_Password = 2;
		public static final int Col_Browser = 3;
		public static final int Col_LastName = 4;
		public static final int Col_Result = 5;
		public static final String Path_ScreenShot = "D://BUSINESS//workspace//HybridFrameworkForDemoSite//Framework//src//Screenshots//";
	}
