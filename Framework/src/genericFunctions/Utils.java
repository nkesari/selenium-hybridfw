package genericFunctions;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {
		public static WebDriver driver = null;
		
	public static WebDriver OpenBrowser(int iTestCaseRow) throws Exception{
		String sBrowserName;
		try{
		sBrowserName = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_Browser);
			if(sBrowserName.equals("IE")){
					File file = new File("E:/BUSINESS/SOFTWARE TESTING HELP/SELENIUM AND JAVA COURSE MATERIAL/Drivers and Jars/IEDriverServer_x64_2.43.0/IEDriverServer.exe");
					System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
					DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer(); 
					ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true); 
					driver = new InternetExplorerDriver(ieCapabilities);
					Log.info("New driver instantiated");
				    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				    Log.info("Implicit wait applied on the driver for 10 seconds");
				    driver.get(Constant.URL);
				    Log.info("Web application launched successfully");
			}else if(sBrowserName.equals("FF")){
					driver = new FirefoxDriver();
					Log.info("New driver instantiated");
				    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				    Log.info("Implicit wait applied on the driver for 10 seconds");
				    driver.get(Constant.URL);
				    Log.info("Web application launched successfully");
					}
			
		}catch (Exception e){
			Log.error("Class Utils | Method OpenBrowser | Exception desc : "+e.getMessage());
		}
		return driver;
	}
	
	public static String getTestCaseName(String sTestCase)throws Exception{
		String value = sTestCase;
		try{
			int posi = value.indexOf("@");
			value = value.substring(0, posi);
			posi = value.lastIndexOf(".");	
			value = value.substring(posi + 1);
			return value;
				}catch (Exception e){
			Log.error("Class Utils | Method getTestCaseName | Exception desc : "+e.getMessage());
			throw (e);
					}
			}

	 public static void waitForElement(WebElement element){
		 
		 WebDriverWait wait = new WebDriverWait(driver, 60);
	     wait.until(ExpectedConditions.elementToBeClickable(element));
	 	}
		
	 public static boolean verifyTextOnPage(String str){
		 if(driver.getPageSource().contains(str)){
			 return true;
		 }else{
			 return false;
		 }
	}
			 
	 public static void takeScreenshot(WebDriver driver, String sTestCaseName) throws Exception{
			try{
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(Constant.Path_ScreenShot + sTestCaseName +".jpg"));	
			} catch (Exception e){
				Log.error("Class Utils | Method takeScreenshot | Exception occured while capturing ScreenShot : "+e.getMessage());
				throw new Exception();
			}
		}
	 
	 
	}
