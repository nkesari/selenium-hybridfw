package pageObjectModels;
        import org.openqa.selenium.*;

import genericFunctions.Log;
    public class LogIn_Page extends BaseClass {
           private static WebElement element = null;
        
        public LogIn_Page(WebDriver driver){
            	super(driver);
        }     
        public static WebElement txtbx_UserName() throws Exception{
        	try{
	            element = driver.findElement(By.name("email_address"));
	            Log.info("Username text box is found on the Login Page");
        	}catch (Exception e){
           		Log.error("UserName text box is not found on the Login Page");
           		throw(e);
           		}
           	return element;
            }
        
        public static WebElement txtbx_Password() throws Exception{
        	try{
	        	element = driver.findElement(By.name("password"));
	            Log.info("Password text box is found on the Login page");
        	}catch (Exception e){
        		Log.error("Password text box is not found on the Login Page");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement btn_LogIn() throws Exception{
        	try{
	        	element = driver.findElement(By.id("tdb5"));
	            Log.info("Submit button is found on the Login page");
        	}catch (Exception e){
        		Log.error("Submit button is not found on the Login Page");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement btn_ContinueLogOut() throws Exception{
        	try{
	        	element = driver.findElement(By.cssSelector("#tdb4 > span.ui-button-text"));
	            Log.info("continue the logout button found");
        	}catch (Exception e){
        		Log.error("continue the logout button not found");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement hdr_Login() throws Exception{
        	try{
	        	element = driver.findElement(By.cssSelector("h1"));
	            Log.info("header h1 element found");
        	}catch (Exception e){
        		Log.error("header h1 element not found");
           		throw(e);
           		}
           	return element;
        }
    }
