package pageObjectModels;
        import org.openqa.selenium.*;

import genericFunctions.Log;
    public class ChangeProfile extends BaseClass {
           private static WebElement element = null;
        
        public ChangeProfile(WebDriver driver){
            	super(driver);
        }     
        public static WebElement lnk_ViewChange() throws Exception{
        	try{
	            element = driver.findElement(By.linkText("View or change my account information."));
	            Log.info("view or change my account link found");
        	}catch (Exception e){
           		Log.error("view or change my account link not found");
           		throw(e);
           		}
           	return element;
            }
        
        public static WebElement txtbx_lastName() throws Exception{
        	try{
	        	element = driver.findElement(By.name("lastname"));
	            Log.info("lastname text box found");
        	}catch (Exception e){
        		Log.error("last name text box not found");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement btn_Continue() throws Exception{
        	try{
	        	element = driver.findElement(By.id("tdb5"));
	            Log.info("Continue button found");
        	}catch (Exception e){
        		Log.error("Continue Button Not Found");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement cssVerification() throws Exception{
        	try{
	        	element = driver.findElement(By.className("messageStackSuccess"));
	            Log.info("verification found");
        	}catch (Exception e){
        		Log.error("verification not found");
           		throw(e);
           		}
           	return element;
        }
        
     }
