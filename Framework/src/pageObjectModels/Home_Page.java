package pageObjectModels;
        import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import genericFunctions.Log;
import genericFunctions.Utils;
    public class Home_Page extends BaseClass{
        
    	private static WebElement element = null;
    	
        public Home_Page(WebDriver driver){
            	super(driver);
        }    
        public static WebElement lnk_MyAccount() throws Exception{
            try{ 
	        	 element = driver.findElement(By.linkText("My Account"));
	             Log.info("My Account link is found on the Home Page");
            }catch (Exception e){
           		Log.error("My Acocunt link is not found on the Home Page");
           		throw(e);
           		}
           	return element;
        }
        public static WebElement lnk_LogOut() throws Exception{
            try{
	        	element = driver.findElement(By.linkText("Log Off"));
	            Log.info("Log Out link is found on the Home Page");
            }catch (Exception e){
            	Log.error("Log Out link is not found on the Home Page");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement txt_Keyword() throws Exception{
            try{
	        	element = driver.findElement(By.name("keywords"));
	            Log.info("textbox keywords is found on the Home Page");
            }catch (Exception e){
            	Log.error("text box keyword is not found on the Home Page");
           		throw(e);
           		}
           	return element;
        }
        
        public static WebElement btn_QuickFind() throws Exception{
            try{
	        	element = driver.findElement(By.xpath("//input[@type='image']"));
	            Log.info("Button Quick Find is found on the Home Page");
            }catch (Exception e){
            	Log.error("Button Quick Find is not found on the Home Page");
           		throw(e);
           		}
           	return element;
        }
        
        
        
   }
