package applicationFunctions;

import org.testng.Reporter;

import pageObjectModels.Home_Page;
import pageObjectModels.LogIn_Page;
import genericFunctions.Constant;
import genericFunctions.ExcelUtils;
import genericFunctions.Log;
import genericFunctions.Utils;
     
    // This is called Modularization, when we club series of actions in to one Module
   public class SignIn_Action_Valid {
    	// iTestcaseRow is the row number of our Testcase name in the Test Data sheet
    	// iTestcaseRow is passed as an Argument to this method, so that it can used inside this method
    	 public static void Execute(int iTestCaseRow) throws Exception{
        	
        	// Clicking on the My Account link on the Home Page
        	Home_Page.lnk_MyAccount().click();
        	Log.info("Click action is perfromed on My Account link" );
        	
        	// Storing the UserName in to a String variable and Getting the UserName from Test Data Excel sheet
        	// iTestcaseRow is the row number of our Testcase name in the Test Data sheet
        	// Constant.Col_UserName is the column number for UserName column in the Test Data sheet
        	// Please see the Constant class in the Utility Package
        
        	String sUserName = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_UserName);
        	LogIn_Page.txtbx_UserName().sendKeys(sUserName);
           	Log.info(sUserName+" is entered in UserName text box" );
            
            String sPassword = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_Password);
            LogIn_Page.txtbx_Password().sendKeys(sPassword);
            Log.info(sPassword+" is entered in Password text box" );
            
            LogIn_Page.btn_LogIn().click();
            Log.info("Click action is performed on Submit button");
            
            // I noticed in few runs that Selenium is trying to perform the next action before the complete Page load
            // So I have decided to put a wait on the Logout link element
            // Now it will wait 10 secs separately before jumping out to next step
          
            Utils.waitForElement(Home_Page.lnk_LogOut());
            Home_Page.lnk_LogOut().click();
            LogIn_Page.btn_ContinueLogOut().click();
            
            // This is another type of logging, with the help of TestNg Reporter log
            // This has to be very carefully used, you should only print the very important message in to this
            // This will populate the logs in the TestNG HTML reports
            // I have used this Reporter log just once in this whole module 
            Reporter.log("SignIn Action is successfully perfomred");
            
        }
    }
