package applicationFunctions;

import org.testng.Assert;
import org.testng.Reporter;

import pageObjectModels.BaseClass;
import pageObjectModels.Home_Page;
import pageObjectModels.LogIn_Page;
import genericFunctions.Constant;
import genericFunctions.ExcelUtils;
import genericFunctions.Log;
import genericFunctions.Utils;
     
    // This is called Modularization, when we club series of actions in to one Module
   public class SignIn_Action_InValid {
    	// iTestcaseRow is the row number of our Testcase name in the Test Data sheet
    	// iTestcaseRow is passed as an Argument to this method, so that it can used inside this method
    	 public static void Execute(int iTestCaseRow) throws Exception{
        	
        	// Clicking on the My Account link on the Home Page
        	Home_Page.lnk_MyAccount().click();
        	Log.info("Click action is perfromed on My Account link" );
        	
        	// Storing the UserName in to a String variable and Getting the UserName from Test Data Excel sheet
        	// iTestcaseRow is the row number of our Testcase name in the Test Data sheet
        	// Constant.Col_UserName is the column number for UserName column in the Test Data sheet
        	// Please see the Constant class in the Utility Package
        	String sUserName = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_UserName);
        	
            LogIn_Page.txtbx_UserName().sendKeys(sUserName);
            // Printing the logs for what we have just performed
            Log.info(sUserName+" is entered in UserName text box" );
            
            String sPassword = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_Password);
            LogIn_Page.txtbx_Password().sendKeys(sPassword);
            Log.info(sPassword+" is entered in Password text box" );
            
            LogIn_Page.btn_LogIn().click();
            Log.info("Click action is performed on Submit button");
            
           try{
        	   Home_Page.lnk_LogOut().click();
              }catch(Exception ex){
            	  Log.info("Log off link not found");
            	  throw new Exception("Log off link not found");
              }
            
            Reporter.log("SignIn Action is successfully perfomred");
            
        }
    }
