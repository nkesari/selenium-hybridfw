package applicationFunctions;

import org.testng.Assert;
import org.testng.Reporter;

import pageObjectModels.BaseClass;
import pageObjectModels.Home_Page;
import genericFunctions.Constant;
import genericFunctions.ExcelUtils;
import genericFunctions.Log;
import genericFunctions.Utils;
import pageObjectModels.ChangeProfile;
     
    // This is called Modularization, when we club series of actions in to one Module
   public class SearchProduct_Action {
    	// iTestcaseRow is the row number of our Testcase name in the Test Data sheet
    	// iTestcaseRow is passed as an Argument to this method, so that it can used inside this method
    	 public static void Execute(int iTestCaseRow) throws Exception{
        	
    		 String testData = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_LastName);
        	
            Home_Page.txt_Keyword().sendKeys(testData);
            Home_Page.btn_QuickFind().click();
            Log.info("Click action is performed on quick find button");
            
            if (Utils.verifyTextOnPage(testData)){
            	Log.info("Product found on page, search is complete");
    		}else{
            			Log.info("Product not found on page");
            			throw new Exception("product not found on page");
            		}           		
            Reporter.log("Change Profile is successfully perfomred");
            
        }
    }
